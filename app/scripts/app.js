'use strict';

/**
 * @ngdoc overview
 * @name atlantaApp
 * @description
 * # atlantaApp
 *
 * Main module of the application.
 */


angular
    .module('atlantaApp', [
      'ngAnimate',
      'ngCookies',
      'ngResource',
      'ngRoute',
      'ngSanitize',
      'ngTouch',
      'google-maps'
      ])
    .config(function ($routeProvider) {
      $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .when('/login', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl'
      })
      .when('/assistance', {
        templateUrl: 'views/assistance.html',
        controller: 'AssistanceCtrl'
      })
      .when('/da', {
        templateUrl: 'views/da.html',
        controller: 'AssistanceCtrl'
      })
      .when('/declsinistre', {
        templateUrl: 'views/declsinistre.html',
        controller: 'DeclsinistreCtrl'
      })
      .when('/agences', {
        templateUrl: 'views/agences.html',
        controller: 'AgencesCtrl'
      })
      .when('/agence/:ville?', {
        templateUrl: 'views/agence.html',
        controller: 'AgenceCtrl'
      })
      .when('/devis', {
        templateUrl: 'views/devis.html',
        controller: 'DevisCtrl'
      })
      .when('/call', {
        templateUrl: 'views/call.html',
        controller: 'CallCtrl'
      })
      .when('/actu', {
        templateUrl: 'views/actu.html',
        controller: 'ActuCtrl'
      })
      .when('/actuselected', {
        templateUrl: 'views/actuselected.html',
        controller: 'ActuCtrl'
      })
      .when('/result', {
        templateUrl: 'views/result.html',
        controller: 'DevisCtrl'
      })
      .when('/test', {
        templateUrl: 'views/test.html',
        controller: 'TesttCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
    })
    .directive('backButton', function(){
        return {
          restrict: 'A',

          link: function(scope, element, attrs) {
            element.bind('click', goBack);

            function goBack() {
              history.back();
              scope.$apply();
            }
          }
        }
    })
    .factory('GeolocalisationService',function($window , $q ,$rootScope){
      var geoloc= $window.navigator.geolocation;
      return {
        getCurrentGeoloc : function (onsuccess, onerror){
          geoloc.getCurrentPosition(
            function(position){
              $rootScope.$apply(function(){
                onsuccess(position);
                console.log(position);
              })
            }
            ,function(){
              $rootScope.$apply(function(){
                onerror();
              }
              )
            })
          return true;
          }
        }
      })
    .factory('PassingData', function() {
       var savedData = {}
       function set(data) {
         savedData = data;
       }
       function get() {
        return savedData;
       }

       return {
        set: set,
        get: get
       }
     });


