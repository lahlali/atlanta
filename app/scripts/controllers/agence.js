'use strict';

/**
 * @ngdoc function
 * @name atlantaApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the atlantaApp
 */
 angular.module('atlantaApp')
 .controller('AgenceCtrl', function ($scope,$http,$routeParams) {
 	$scope.ville=$routeParams.ville;
 	$scope.loc = {
 		center: {
 			latitude: 45,
 			longitude: -73
 		},
 		zoom: 10
 	};
 	
 	$scope.agences =[];
 	$scope.getAgences =function(){
 		var url ='http://kamikazay.esy.es/hatim';
 		$scope.loader = 1;
 		$http.get(url)
 		.success(function(response){
 			$scope.loader = 0;
 			$scope.agences = response; // response.agences
 			$scope.map.center.latitude=30.420748;
 			$scope.map.center.longitude=-9.559324;
 			$scope.$apply();
 		})
 		.error(function(){
 			$scope.loader = 0;
 			alert('Verfifier Votre Connection');
 		});
 	}
 	$scope.getAgences();
 });

 /***************
	Json retour 
	{
		latitude :
		logitude :
		agences : [
			{
				name:
				personnel :
				latitude :
				longitude :
			}
		]
	}


 **/