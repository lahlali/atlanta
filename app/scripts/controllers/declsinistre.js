'use strict';

/**
 * @ngdoc function
 * @name atlantaApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the atlantaApp
 */
angular.module('atlantaApp')
  .controller('DeclsinistreCtrl', function ($scope,GeolocalisationService,$http) {
    $scope.loader = 0;
    $scope.locationName= '';
  	$scope.parametres= '';
	$scope.map = {
	    center: {
	        latitude: 45,
	        longitude: -73
	    },
	    zoom: 10
	};
	var geocoder = new google.maps.Geocoder();
	

    $scope.reverser = function (){
    	var latlng = new google.maps.LatLng($scope.map.center.latitude,$scope.map.center.longitude);
    	geocoder.geocode({'latLng': latlng}, function(results, status) {
	      if (status == google.maps.GeocoderStatus.OK) {
	        if (results[1]) {
	          //map.setZoom(11);
	          $scope.map.zoom=11;
	          $scope.locationName=results[1].formatted_address;
	        }
	      } else {
	        alert("Geocoder failed due to: " + status);
	      }
	    });
    }

    $scope.geolocate = function(){
    	GeolocalisationService.getCurrentGeoloc(function (position){
	  		$scope.map.center.latitude=position.coords.latitude;
	  		$scope.map.center.longitude=position.coords.longitude;  
	  		$scope.reverser();
	  		console.log($scope.locationName);
		  	},function(){
		  		alert('Activer le gps');
		  	});
    }

    $scope.send = function(){
    	var url ='http://WWW.google.com';
        $scope.loader = 1;
        $http.post(url,$scope.parametres)
            .success(function(){
                $location.path('/da');
            })
            .error(function(){
                alert('Verifier Votre Connection');$location.path('/da');
            });
    }







    $scope.reverser();
  });
