'use strict';

/**
 * @ngdoc function
 * @name atlantaApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the atlantaApp
 */
angular.module('atlantaApp')
  .controller('CallCtrl', function ($scope) {
    $scope.nums= [
	    {
	    	name :"Police",
	    	num :"19"
	    },
	    {
	    	name :"Gendarmerie Royale",
	    	num :"177"
	    },
	    {
	    	name :"Pompiers",
	    	num :"15"
	    },
	    {
	    	name :"Assistance Atlanta",
	    	num :"+212 021 025 024"
	    },
	    {
	    	name :"Support Atlanta",
	    	num :"+212 025 465 585"
	    },
	    {
	    	name :"Autoroutes Maroc",
	    	num :"50 50"
	    }
    ];
  });
