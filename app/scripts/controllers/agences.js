'use strict';

/**
 * @ngdoc function
 * @name atlantaApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the atlantaApp
 */

 /*******
	optimisation autour en deuxieme lieu 
	faire appel a la geoloc quand c'est necessaire : rend lent le device ;

 ****************/
angular.module('atlantaApp')
  .controller('AgencesCtrl', function ($scope,GeolocalisationService,$http,$location) {

  	
  	$scope.curville ='';
  	$scope.agencesAutour= [];

  	$scope.getAutour =function(){
  		var url ='http://kamikazay.esy.es/hatim';
  		$scope.panel=0;
  		GeolocalisationService.getCurrentGeoloc(function (position){
	  		$scope.map.center.latitude=position.coords.latitude;
	  		$scope.map.center.longitude=position.coords.longitude;  
	  		$scope.loader = 1;
	        $http.get(url)
	            .success(function(response){
	                $scope.loader = 0;
	                $scope.agencesAutour = response;
	                $scope.$apply();
	            })
	            .error(function(){
	                $scope.loader = 0;
	                alert('Verifier Votre Connection');
	            });
		  	},function(){
		  		alert('Activer le gps');
		  	});
    }

  	$scope.panel=1;
  	$scope.go =0;
    $scope.agences =[];

	$scope.map = {
	    center: {
	        latitude: 45,
	        longitude: -73
	    },
	    zoom: 10
	};

	$scope.villes= [
		  {
		    "Ville":"Agadir"
		  },
		  {
		    "Ville":"Al Hoceima"
		  },
		  {
		    "Ville":"Aruit"
		  },
		  {
		    "Ville":"Attaouia"
		  },
		  {
		    "Ville":"Azilal"
		  },
		  {
		    "Ville":"Bab Berred"
		  },
		  {
		    "Ville":"Ben Guerir"
		  },
		  {
		    "Ville":"Beni Mellal"
		  },
		  {
		    "Ville":"Benslimane"
		  },
		  {
		    "Ville":"Berkane"
		  },
		  {
		    "Ville":"Berrechid"
		  },
		  {
		    "Ville":"Casablanca"
		  },
		  {
		    "Ville":"Driouech"
		  },
		  {
		    "Ville":"El Hajeb"
		  },
		  {
		    "Ville":"El Jadida"
		  },
		  {
		    "Ville":"Essaouira"
		  },
		  {
		    "Ville":"Fes"
		  },
		  {
		    "Ville":"Guelmim"
		  },
		  {
		    "Ville":"Guercif"
		  },
		  {
		    "Ville":"Inzegane"
		  },
		  {
		    "Ville":"Jorf El Melha"
		  },
		  {
		    "Ville":"Kalaa Sraghna"
		  },
		  {
		    "Ville":"Kasbat Tadla"
		  },
		  {
		    "Ville":"Kenitra"
		  },
		  {
		    "Ville":"Ksar El Kebir"
		  },
		  {
		    "Ville":"Laayoune"
		  },
		  {
		    "Ville":"Marrakech"
		  },
		  {
		    "Ville":"Mechra Belksiri"
		  },
		  {
		    "Ville":"Meknes"
		  },
		  {
		    "Ville":"Midelt"
		  },
		  {
		    "Ville":"Mohammedia"
		  },
		  {
		    "Ville":"Nador"
		  },
		  {
		    "Ville":"Oujda"
		  },
		  {
		    "Ville":"Ouled Teima"
		  },
		  {
		    "Ville":"Rabat"
		  },
		  {
		    "Ville":"Rommani"
		  },
		  {
		    "Ville":"Safi"
		  },
		  {
		    "Ville":"Sale"
		  },
		  {
		    "Ville":"Settat"
		  },
		  {
		    "Ville":"Sidi Bennour"
		  },
		  {
		    "Ville":"Sidi Slimane"
		  },
		  {
		    "Ville":"Souk Sebt"
		  },
		  {
		    "Ville":"Tanger"
		  },
		  {
		    "Ville":"Taourirt"
		  },
		  {
		    "Ville":"Taza"
		  },
		  {
		    "Ville":"Temara"
		  },
		  {
		    "Ville":"Tetouan"
		  },
		  {
		    "Ville":"Tifelt"
		  }
		];

	
  });

/*
	Json Villes 
	[
		{
			ville:
			nbreagences:
		}
	]

*/