'use strict';

/**
 * @ngdoc function
 * @name atlantaApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the atlantaApp
 */
angular.module('atlantaApp')
  .controller('DevisCtrl', function ($scope,$http,$location) {


    $scope.step=0;
    $scope.loader = 0;
    $scope.choixTarif ='';
    $scope.dateMiseEnCirculation='';
    $scope.dateEffet='';
    $scope.valeurNeuve='';
    $scope.valeurVenale='';
    $scope.valeurGlaces='';
    $scope.carburant='';
    $scope.puissance='';
    $scope.nbPlaces='';
    $scope.immatriculation='';
    $scope.flagBonConducteur='';

    $scope.flagRc ='';
    $scope.flagDr ='';
    $scope.flagPc ='';
    $scope.flagPta ='';
    $scope.formulePta ='';
    $scope.formulePtaFonct ='';
    $scope.formulePtaSayidati ='';
    $scope.flagVol ='';
    $scope.flagIncendie ='';
    $scope.flagDc ='';
    $scope.optionDc ='';
    $scope.optionDcFonct ='';
    $scope.optionDcSayidati ='';
    $scope.flagTierce ='';
    $scope.optionTierce ='';
    $scope.flagInondation ='';
    $scope.flagBg ='';
    $scope.optionBg ='';

    $scope.parametres='';


    $scope.submit =function(){
        var url ='http://WWW.google.com';
        $scope.loader = 1;
        //$http.post(url,$scope.parametres)
        $http.get(url)
            .success(function(){
                $scope.loader = 0;
                $scope.step++;
                $location.path('/result');
            })
            .error(function(){
                $scope.loader = 0;
                alert('Verifier Votre Connection');
            });

    }

    $scope.previousstep= function(){
        if($scope.step>0){
            $scope.step--;
        }
        
    }
    $scope.nextstep= function(){
        console.log(construireParametres());
        switch($scope.step) {
            case 0:
                if ($scope.choixTarif =='' || $scope.dateMiseEnCirculation=='' || $scope.dateEffet==''|| $scope.valeurNeuve=='' || $scope.valeurVenale=='' || $scope.valeurGlaces==''){
                    alert('Veuiller remplire tout les champs');
                    $scope.step++;
                    return false;
                }
                break;
            case 3:
                
                break;
        }
        $scope.step++;
        return true;
    }

    function construireParametres ()
            {
                // Construire les variables à passer en POST.
                $scope.parametres = "immatriculation=" + document.getElementById("immatriculation").value 
                            + "&puissance=" + document.getElementById("puissance").value 
                            + "&carburant=" + document.getElementById("carburant").value 
                            + "&choixTarif=" + document.getElementById("choixTarif").value 
                            + "&nbPlaces=" + document.getElementById("nbPlaces").value 
                            + "&formulePta=" + document.getElementById("formulePta").value 
                            + "&formulePtaFonct=" + document.getElementById("formulePtaFonct").value  
                            + "&formulePtaSayidati=" + document.getElementById("formulePtaSayidati").value 
                            + "&valeurVenale=" + document.getElementById("valeurVenale").value 
                            + "&optionDc=" + document.getElementById("optionDc").value 
                            + "&optionDcFonct=" + document.getElementById("optionDcFonct").value  
                            + "&optionDcSayidati=" + document.getElementById("optionDcSayidati").value 
                            + "&valeurNeuve=" + document.getElementById("valeurNeuve").value 
                            + "&optionTierce=" +  document.getElementById("optionTierce").value 
                            + "&optionBg=" + document.getElementById("optionBg").value 
                            + "&valeurGlaces=" + document.getElementById("valeurGlaces").value 
                            + "&dateMiseEnCirculation=" + document.getElementById("dateMiseEnCirculation").value 
                            + "&dateEffet=" + document.getElementById("dateEffet").value;

                if (document.getElementById("flagBonConducteur").checked) {
                    $scope.parametres = $scope.parametres + "&flagBonConducteur=on" ;
                }
                if (document.getElementById("flagDr").checked) {
                    $scope.parametres = $scope.parametres + "&flagDr=on" ;
                }
                if  (document.getElementById("flagPta").checked) {
                    $scope.parametres = $scope.parametres + "&flagPta=on" ;
                }
                if  (document.getElementById("flagPta").checked) {
                    $scope.parametres = $scope.parametres + "&flagPta=on" ;
                }
                if  (document.getElementById("flagVol").checked) {
                    $scope.parametres = $scope.parametres + "&flagVol=on" ;
                }
                if  (document.getElementById("flagIncendie").checked) {
                    $scope.parametres = $scope.parametres + "&flagIncendie=on" ;
                }
                if  (document.getElementById("flagDc").checked) {
                    $scope.parametres = $scope.parametres + "&flagDc=on" ;
                }
                if  (document.getElementById("flagTierce").checked) {
                    $scope.parametres = $scope.parametres + "&flagTierce=on" ;
                }
                if  (document.getElementById("flagInondation").checked) {
                    $scope.parametres = $scope.parametres + "&flagInondation=on" ;
                }
                if  (document.getElementById("flagBg").checked) {
                    $scope.parametres = $scope.parametres + "&flagBg=on" ;
                }

                return $scope.parametres;
            }

  });
