'use strict';

/**
 * @ngdoc function
 * @name atlantaApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the atlantaApp
 */
angular.module('atlantaApp')
  .controller('ActuCtrl', function ($scope,$location,PassingData) {
  	$scope.panel = 0;
    $scope.temp = PassingData.get();
    $scope.actus= [
    	{
            title : "AUTO+ 1ERE MAIN",
            desc : "Votre véhicule vous est cher ? Vous le chouchoutez comme au premier jour et désirez une couverture taillée sur-mesure ? Vous avez tapé à la bonne porte ! En exclusivité chez Atlanta, le produit Auto+, avec sa solution 1ERE MAIN destinée aux véhicules qui n’ont jamais changé de propriétaire, offre une panoplie de garanties pour une couverture maximale.",
            image :"autoplus.png"
        },
        {
            title : "AUTO+ MOUWADAF",
            desc : "Le produit Auto+, avec sa solution MOUWADAF destinée aux Fonctionnaires des secteurs Public et Semi Public ainsi qu’aux fonctionnaires des Offices, est spécialement adapté à vos besoins et vous permet de bénéficier d’une couverture optimale à des tarifs très étudiés ainsi que des prestations d’assistance très complètes.",
            image :"MOUWADAF.png"
        },
        {
            title : "AUTO+ SAYIDATI",
            desc : "ATLANTA chouchoute les femmes et leur offre un produit d’assurance automobile taillé à leurs exigences, complet, compétitif et citoyen : Auto+ SAYIDATI.",
            image :"SAYIDATI.png"
        },
        {
            title : "MASKANI",
            desc : "Vous possédez un appartement, une villa ou une maison individuelle ?  En propriété, en copropriété ou en location ? Votre logement abrite du mobilier d’art, des objets et bibelots de grande valeur ? Parce qu’aucune habitation ne ressemble à une autre, MASKANI a été conçu afin de vous satisfaire et être adapté à votre situation, quel que soit votre patrimoine.",
            image :"atlanta.png"
        },
        {
            title : "EPARGNE IHTIATE",
            desc : "Assurer un bel avenir à ses enfants est sans doute la préoccupation majeure de tout parent.\n Garantie de Base : Epargne IHTIATEconsiste à cotiser périodiquement auprès de sa banque une somme d’argent pour pouvoir disposer, à une date convenue, d’un Capital ou d’une rente périodique, ou un mélange des deux, qui aidera à financer une bonne partie des études de son enfant.\n Garantie Facultative : En souscrivant à la garantie facultative qui agit en cas de décès ou d’invalidité absolue et définitive du parent,  l’enfant bénéficie de la continuité de versement des cotisations et d’une rente trimestrielle équivalente jusqu’à l’âge de 18 ans.",
            image :"IHTIATE.png"
        },
        {
            title : "RETRAITE ISTIMRAR",
            desc :"Retraite ISTIMRAR vise à constituer une épargne lors de la vie active en vue de disposer d’une rente à la retraite. C'est une forme d'épargne par Capitalisation. Elle est constituée à partir des versements périodiques. Les sommes sont conservées jusqu'au départ à la retraite et ensuite versées sous forme de Capital, transformées en rente à vie ou limitée dans le temps ou bien une combinaison entre le Capital et la rente.",
            image :"ISTIMRAR.png"
        }

    ];
    $scope.offres= [
    	{
    		title : "lorem",
    		desc : "ipsum"
    	},
    	{
    		title : "lorem",
    		desc : "ipsum"
    	}
    ];
    $scope.videos= [
    	{
    		title : "lorem",
    		desc : "ipsum"
    	},
    	{
    		title : "lorem",
    		desc : "ipsum"
    	}
    ];

    $scope.getActus = function(){
    	$scope.panel = 0;
    }

    $scope.getVideos = function(){
    	$scope.panel = 1;
    }

    $scope.getOffres = function(){
        $scope.panel = 2;
    }
    $scope.goto = function($item){
        $scope.temp = $item;
        console.log($item);
        PassingData.set($item);
        $location.path('/actuselected');
    }


  });


