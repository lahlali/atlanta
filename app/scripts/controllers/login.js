'use strict';

/**
 * @ngdoc function
 * @name atlantaApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the atlantaApp
 */
angular.module('atlantaApp')
  .controller('LoginCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
